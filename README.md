# SimpleState State Machine
A simple state machine for [Godot](https://godotengine.org) 4.0.

Go to the [wiki](https://gitlab.com/addons-by-aura/simple-state/-/wikis/home) for docs.

## Credits
All the code was written by me (@auratheenby), but the ideas for some of the specific features (mainly the timers, and the looping in `AnimationState` and `SequenceState`) came from [this addon](https://gitlab.com/atn_games/xsm2-g4), though I simplified their implementation.

I got the icons for most of the classes from these assets of [Kenney's](kenney.nl):
[1](https://kenney.nl/assets/board-game-icons)
[2](https://kenney.nl/assets/game-icons),
and one came from [Pictogrammers](https://pictogrammers.com/library/mdi/).
